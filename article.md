---
title: "Analýza ČRo: Sobotka u voličů ČSSD neztrácí důvěru. Nedůvěřují Dienstbierovi"
perex: "Říjnová data z průzkumu veřejného mínění ukazují, že kauzy posledních měsíců Sobotkovi neubližují, mezi lídry stran patří k těm nejsilnějším. Vůbec nejdůvěryhodnějším lídrem je u svých voličů Andrej Babiš. Nejméně důvěryhodní jsou lídři pravicových stran Kalousek a Fiala."
description: "Říjnová data z průzkumu veřejného mínění ukazují, že kauzy posledních měsíců Sobotkovi neubližují, mezi lídry stran patří k těm nejsilnějším. Vůbec nejdůvěryhodnějším lídrem je u svých voličů Andrej Babiš. Nejméně důvěryhodní jsou lídři pravicových stran Kalousek a Fiala."
authors: ["Jan Boček"]
published: "29. listopadu 2016"
coverimg: https://interaktivni.rozhlas.cz/duvera-politikum/media/cover.jpg
coverimg_note: "Bohuslav Sobotka a Milan Chovanec. Foto Martin Svozílek|ČRo"
socialimg: https://interaktivni.rozhlas.cz/duvera-politikum/media/socialimg.png
url: "duvera-politikum"
libraries: [jquery, highcharts]

recommended:
  - link: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/
    title: Jak volili vaši sousedi?
    perex: Prohlédněte si nejpodrobnější mapu volebních výsledků
    image: https://interaktivni.rozhlas.cz/media/51445ce4a3c687cbb2d35b797e18e359/600x_.jpg
  - link: https://interaktivni.rozhlas.cz/horacek-na-startu/
    title: Horáček a spol. na startu. Co čeká prezidentské kandidáty?
    perex: Do boje o Hrad se přihlásili první zájemci. Připomeňte si, čím musí projít.
    image: https://interaktivni.rozhlas.cz/media/3768d38b68ed061b0612971f6e18892f/600x_.jpg
  - link: http://www.rozhlas.cz/zpravy/technika/_zprava/stoleti-brouka-hitlerovo-lidove-auto-prezilo-valku-a-svet-si-ho-zamiloval--1584151
    title: ČSSD ztrácela napříč republikou, ANO triumfuje
    perex: Mapa zisků v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011.
    image: https://interaktivni.rozhlas.cz/media/c4d8ceec27f29211c75af1aab6ae5cc6/600x_.jpg
---

Bohuslav Sobotka měl v říjnu podporu 78 procent voličů své strany. Jeho straničtí konkurenti na tom byli hůř: Lubomíru Zaorálkovi důvěřovalo 54 procent voličů ČSSD a místopředsedovi Milanu Chovancovi 43 procent voličů. Tomu navíc téměř stejná část voličů podle říjnových čísel nedůvěřuje. Ostatní politici strany mají podporu voličů srovnatelnou nebo ještě nižší.

Ukazuje to analýza Českého rozhlasu nad [daty agentury CVVM](http://archiv.soc.cas.cz/elektronicky-datovy-katalog-csda-system-nesstar), která pravidelně sleduje důvěru přibližně třem desítkám vrcholných politiků. Namísto důvěry politikům mezi všemi voliči jsme se ovšem zaměřili na to, jakou mají politici důvěru *mezi voliči své strany*. Výsledné číslo tedy vypovídá o jejich důvěře mezi voliči strany a může být důležité také pro pozici politika uvnitř své strany.

*Pro přehlednost jsou v grafu znázorněni pouze tři zmínění politici ČSSD, ostatní zobrazíte kliknutím na jejich jméno. Plná čára ukazuje důvěru, tečkovaná nedůvěru, zbytek politika nezná nebo se o důvěře neumí rozhodnout.*

<aside class="big">
  <iframe src="https://samizdat.cz/data/stepeni-2016/charts/lidriCSSD.html" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

„Při vší své nevýraznosti a necharizmatičnosti je Sobotka v současnosti prostě nejviditelnější a nejvýznamnější figura v ČSSD, která spojuje post předsedy strany a zároveň předsedy vlády,“ vysvětluje Jan Červenka, analytik agentury CVVM. „Komu jinému už by voliči strany měli důvěřovat?“

„Stabilně vysoká podpora předsedy ovšem kontrastuje se zhoršujícími se preferencemi strany,“ pokračuje Červenka. „Naznačuje to, že Sobotka může mít problém přitahovat voliče. Přitom ekonomická situace i životní úroveň mají zjevně vzestupnou úroveň a rozhodně se nedá říct, že by ČSSD neprosadila nic ze svého programu nebo že by její politika byla neúspěšná. Ale body za to sbírá v první řadě Andrej Babiš a ANO.“

## Dienstbierovo „liberální“ křídlo současné voliče neláká

Vysoká důvěra voličů se odráží i v aktuálním boji o pozici nového předsedy strany. Sobotka dosud [získal podporu tří krajů ze třech možných](http://echo24.cz/a/ijpFU/sobotka-sbira-po-krajich-nominace-na-predsedu-zatim-je-uspesny).

Naopak oba ministři za ČSSD, které [Sobotka v polovině listopadu odvolal](http://www.rozhlas.cz/zpravy/domaci/_zprava/zmeny-ve-vlade-nemecka-nahradi-reditel-fn-motol-ludvik-dienstbiera-poslanec-chvojka--1668756), Svatopluk Němeček a Jiří Dienstbier, mají u voličů nízkou důvěru. Dienstbier je jediným z politiků strany, kterému větší část voličů explicitně nevěří.

Podobně stabilní podporu jako Sobotka má mezi voliči ČSSD už pouze jeden politik – prezident Miloš Zeman. V říjnu mu důvěřovalo 74 procent voličů této strany, nedůvěřovalo 21 procent.

„Miloš Zeman má mezi voliči ČSSD postavení plně srovnatelné s Bohuslavem Sobotkou,“ upozorňuje Červenka. „Zeman je na tom ale vesměs lépe u voličů jiných stran a zejména u těch, kteří volí protestně – tedy KSČM, částečně ANO, SPD nebo Úsvit. Je tedy možné, že pokud by on byl předsedou ČSSD místo Bohuslava Sobotky, měla by strana větší šanci konkurovat ANO.“

## Ostatní lídři: Babiš je nedotknutelný, Kalousek nedůvěryhodný

Mezi lídry ostatních stran je nejsilnější Andrej Babiš. V říjnu měl důvěru 91 procent voličů ANO.

<aside class="big">
  <iframe src="https://samizdat.cz/data/stepeni-2016/charts/lidri.html" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

„Pozice ministra financí je v českém prostředí vždy klíčová a v kombinaci s dlouhodobou prací na zviditelňování nese své výsledky,“ tvrdí Michal Pink, politolog z Fakulty sociálních studií Masarykovy univerzity. „Důležitou vlastností je rovněž skutečnost, že se ve vládě chová dost často jako opoziční politik – přestože je v koalici – a tím na sebe poutá pozornost.“

„Dobrým příkladem je Babišův [postoj k reorganizaci policie v červnu tohoto roku](https://interaktivni.rozhlas.cz/vymeny-v-policii/), který vedl až ke koaliční krizi,“ pokračuje Pink. „Jakmile dostane možnost se zviditelnit, okamžitě se o to snaží. To je nedílná součást populistických stran.“

Ve stejném měsíci se na něj dotáhl i šéf KDU-ČSL Pavel Bělobrádek.

„Předseda strany se silně ohraničeným elektorátem a pevnou strukturou bude oblíben skoro vždy,“ vysvětluje Michal Pink. „Navíc má funkci místopředsedy vlády, na českého politika dokáže i slušně mluvit a vystupovat, takže vysoká důvěra u voličů strany je logická. Za skokem v říjnu můžou stát krajské volby, kde se Bělobrádek zviditelnil ve volebních debatách.“

Lídr ODS Petr Fiala a zejména předseda TOP 09 Miroslav Kalousek naopak patří mezi svými voliči k méně důvěryhodným politikům.

„Pokud jde o pozici Miroslava Kalouska, ten si s sebou samozřejmě nese svoji politickou minulost a politickou pověst, která z něj dělá v současnosti politika se zdaleka nejvyšším podílem nedůvěry vůbec,“ říká Jan Červenka z CVVM. „Problémem přitom je, že se nedostavil ani žádný výraznější úspěch v nedávných volbách, který by mohl Kalouska nějak rehabilitovat v očích voličů.“

„V případě Petra Fialy problémem jistě není jeho politická minulost,“ dodává Červenka. „Jako politolog a člověk z akademického prostředí, který do politiky aktivně vstoupil jako ‚záchranář‘ zdevastované ODS teprve nedávno, žádnou nemá. Petra Fialu voliči vlastně moc neznají. K tomu se přidává i to, že Petr Fiala je dost beznadějný suchar, který jen těžko dokáže voliče
jakkoli uchvacovat. Dokonce i kdyby říkal mnohem zajímavější věci, než obvykle říká.“